/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders.filebackedqueue;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import custom.appenders.LogValidity;
import custom.appenders.exceptions.ResendFileException;

/**
 * ResendRecord handles the recording and resending of disk queued messages. It also handles the cleaning up after a successful
 * resend has taken place.
 * 
 * @author Chris Culnane
 * 
 */
public class ResendRecord {

  /**
   * Static logger
   */
  private static final Logger logger                       = LoggerFactory.getLogger(ResendRecord.class);

  /**
   * Constant JSON Field for filePath to BackupFile
   */
  public static final String  RESENDRECORD_FILE_PATH       = "filePath";

  /**
   * Constant JSON Field that records the number of lines resent
   */
  public static final String  RESENDRECORD_LINES_RESENT    = "resent";

  /**
   * Constant JSON Field that records whether the resend is complete
   */
  public static final String  RESENDRECORD_RESENT_COMPLETE = "completed";

  /**
   * String prefix to use on resend records
   */
  public static final String  RESEND_PREFIX                = "resendRecord";

  /**
   * String suffix to use on resend records
   */
  public static final String  RESEND_SUFFIX                = ".resend";

  /**
   * Underlying File containing the ResendRecord
   */
  private File                resendRecord;

  /**
   * BufferedReader for reading the underlying BackupFile
   */
  private BufferedReader      recordReader                 = null;

  /**
   * String containing the last line read from the BackupFile
   */
  private String              lastLine                     = null;

  /**
   * JSONObject that represents the ResendRecord
   */
  private JSONObject          record;

  /**
   * Content/Type constant for Post request
   */
  private static final String CONTENT_TYPE                 = "application/json";

  /**
   * Static method to create a new ResendRecord from the specified File that should point to a BackupFile and stores the new
   * ResendRecord in the specified directory
   * 
   * @param backupFile
   *          File that points to underlying BackupFile
   * @param resendRecordsDir
   *          File that points to the ResendRecord directory, where a new resend record will be created
   * @return String containing the path to the newly created ResendRecord
   * @throws ResendFileException
   */
  public static final String createNewRecord(File backupFile, File resendRecordsDir) throws ResendFileException {
    try {
      JSONObject newRecord = new JSONObject();
      newRecord.put(RESENDRECORD_FILE_PATH, backupFile.getAbsolutePath());
      File newFile = File.createTempFile(RESEND_PREFIX, RESEND_SUFFIX, resendRecordsDir);
      saveJSON(newRecord, newFile);
      logger.info("Created new ResendRecord for {}, stored in {}", backupFile.getAbsolutePath(), newFile.getAbsolutePath());
      return newFile.getAbsolutePath();
    }
    catch (IOException | JSONException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception creating new resend record", e);
    }
  }

  /**
   * Saves the specified JSONObject to the resend record file
   * 
   * @param json
   *          JSONObject to save to the file
   * @param resendRecord
   *          File to save JSON data into
   * @throws ResendFileException
   */
  private static void saveJSON(JSONObject json, File resendRecord) throws ResendFileException {
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(resendRecord));
      bw.write(json.toString());
    }
    catch (IOException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception whilst saving resend record", e);
    }
    finally {
      if (bw != null) {
        try {
          bw.close();
        }
        catch (IOException e) {
          logger.error("Exception closing JSON BufferedWriter", e);
        }
      }

    }
  }

  /**
   * Constructor for creating a new ResendRecord from an existing file path. If creating a new ResendRecord createNewRecord should
   * be used instead.
   * 
   * @param resendRecordPath
   *          String path to resend record to reload
   * @throws ResendFileException
   */
  public ResendRecord(String resendRecordPath) throws ResendFileException {
    logger.info("Reloading resend record from: {}", resendRecordPath);
    this.resendRecord = new File(resendRecordPath);
    this.record = this.readJSONObject(this.resendRecord);

  }

  /**
   * Cleans up the resend record after completing a resend. It will close any open streams and delete the backupfile and resend
   * record
   */
  public synchronized void cleanup() {
    this.closeStream();
    try {
      File backupFileRecord = new File(this.record.getString(RESENDRECORD_FILE_PATH));

      if (!backupFileRecord.delete()) {
        logger.warn("Failed to delete backup file: {}", backupFileRecord.getAbsolutePath());
      }
    }
    catch (JSONException e) {
      logger.error("Exception in JSON ResendRecord", e);
    }
    if (!this.resendRecord.delete()) {
      logger.warn("Failed to delete ResendRecord: {}, this could lead to attempts to resend it",
          this.resendRecord.getAbsolutePath());
    }

  }

  /**
   * Closes the underlying backup file reader, if it has been opened.
   */
  private void closeStream() {
    if (this.recordReader != null) {
      try {
        this.recordReader.close();
        this.recordReader = null;
      }
      catch (IOException e) {
        logger.error("Exception when closing the backup file reader", e);
      }
    }
  }


  

  /**
   * Creates an HTTPPost containing the log entry that is due to be resent
   * 
   * @param line
   *          String containing the log entry to be resent
   * @param url
   *          String containing the url to send the request to
   * @return HttpPost object containing entity and url
   * @throws UnsupportedEncodingException
   */
  private HttpPost createPost(String line, String url) throws UnsupportedEncodingException {
    HttpPost postRequest = new HttpPost(url);
    StringEntity input = new StringEntity(line);
    input.setContentType(CONTENT_TYPE);
    postRequest.setEntity(input);
    return postRequest;
  }

  /**
   * Increments the number of resent lines and saves the record to disk
   * 
   * @throws JSONException
   * @throws ResendFileException
   */
  private void incrementResentLines() throws JSONException, ResendFileException {
    this.record.increment(RESENDRECORD_LINES_RESENT);
    this.saveRecord();
  }

  /**
   * Initialises a reader object for the underlying BackupFile. If some lines have already been resent (signified by a positive
   * value in resent, those lines are pre-read so the next line to be read is the next line to be sent. If in the process of
   * pre-reading the lines we get to the end of the file we return false to indicate there is nothing to resend. Otherwise we resend
   * true to indicate the reader is initialised and ready to be read from
   * 
   * @return boolean true if the read is initialised and ready to be read from, false if all the content has already been sent
   * @throws ResendFileException
   */
  private boolean initReader() throws ResendFileException {
    String filePath = "";
    try {
      filePath = this.record.getString(RESENDRECORD_FILE_PATH);
      File backupFileRecord = new File(filePath);
      this.recordReader = new BufferedReader(new FileReader(backupFileRecord));
      int lineCount = this.record.optInt(RESENDRECORD_LINES_RESENT, 0);
      if (lineCount > 0) {
        for (int i = 0; i < lineCount; i++) {
          if (this.recordReader.readLine() == null) {
            return false;
          }
        }
      }
      return true;
    }
    catch (JSONException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception whilst trying to read resend record", e);
    }
    catch (FileNotFoundException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Underlying file does not exist:" + filePath, e);
    }
    catch (IOException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception whilst trying to read resend data file", e);
    }

  }

  /**
   * Reads the next line from the backup file and sends it via the HttpClient. If the previous send failed it will attempt to resend
   * the line until it succeeds. If the end of the file is reached it will return COMPLETE.
   * 
   * @param client
   *          CloseableHttpClient to use for sending the the log entry to the server
   * @param url
   *          String url of server
   * @return ReadAndSendResult - either COMPLETE, FAIL or SENT
   * @throws ResendFileException
   */
  public synchronized ReadAndSendResult readAndSend(CloseableHttpClient client, String url) throws ResendFileException {
    if (this.recordReader == null) {

      if (this.record.optBoolean(RESENDRECORD_RESENT_COMPLETE, false) || !this.initReader()) {
        // Check if the record is complete and we have finished sending
        this.saveRecord();
        this.closeStream();
        return ReadAndSendResult.COMPLETE;
      }
    }
    try {
      // Check if there is an existing last line, if not, read the next line
      if (this.lastLine == null) {
        this.lastLine = this.recordReader.readLine();
      }
      if (this.lastLine == null) {
        //If the last line is still null we have readed the end of the file, close and update the record
        this.record.put(RESENDRECORD_RESENT_COMPLETE, true);
        this.saveRecord();
        this.closeStream();
        return ReadAndSendResult.COMPLETE;
      }
      else {
        try {
          //Prepare and send post request
          CloseableHttpResponse response = client.execute(this.createPost(this.lastLine, url));
          try {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
              EntityUtils.consumeQuietly(entity);
            }
            if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 202) {
              this.lastLine = null;
              //Increment the line count
              this.incrementResentLines();
              return ReadAndSendResult.SENT;
            }
            else {
              return ReadAndSendResult.FAIL;
            }
          }
          finally {
            response.close();
          }
        }
        catch (IOException e) {
          return ReadAndSendResult.FAIL;
        }

      }
    }
    catch (IOException | JSONException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception whilst reading underlying file", e);
    }

  }

  /**
   * Utility method for reading a JSONObject from a file
   * @param file File to load JSONObject from
   * @return JSONObject loaded from the file
   * @throws ResendFileException
   */
  private JSONObject readJSONObject(File file) throws ResendFileException {
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(this.resendRecord));
      String line = null;
      StringBuffer buffer = new StringBuffer();
      while ((line = br.readLine()) != null) {
        buffer.append(line);
      }
      JSONObject obj = new JSONObject(buffer.toString());
      return obj;
    }
    catch (JSONException | IOException e) {
      LogValidity.getInstance().taintLog();
      throw new ResendFileException("Exception whilst trying to load the resend file information", e);
      
    }
    finally {
      if (br != null) {
        try {
          br.close();
        }
        catch (IOException e) {
          logger.error("Exception closing stream",e);
        }
      }
    }
  }

  /**
   * Saves the ResendRecord back to file using the utility saveJson method
   * @throws ResendFileException
   */
  private void saveRecord() throws ResendFileException {
    ResendRecord.saveJSON(this.record, this.resendRecord);
  }

}
