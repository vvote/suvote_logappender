/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders.filebackedqueue;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import custom.appenders.LogValidity;
import custom.appenders.exceptions.ResendFileException;

/**
 * BackupMonitor managers the writing to the file based backup queue and the resending of existing messages. It also handles
 * reloading of disk cached messages following a restart
 * 
 * @author Chris Culnane
 * 
 */
public class BackupMonitor {

  /**
   * Static logger
   */
  private static final Logger logger            = LoggerFactory.getLogger(BackupMonitor.class);
  /**
   * File that keeps a reference to the cacheDir - this where we store the backup files
   */
  private File                cacheDir;

  /**
   * File that keeps a reference to the ResendRecord directory - where we store all the resend records
   */
  private File                resendRecords;

  /**
   * The current BackupFile, or null if no BackupFile is currently in use
   */
  private BackupFile          backupFile        = null;

  /**
   * Folder name for resendRecords, stored in the cacheDir
   */
  public static final String  RESEND_RECORDS    = "resendRecords";

  /**
   * Executor service for resending messages, once a successful send has taken place
   */
  private ExecutorService     resenderExe       = Executors.newSingleThreadExecutor();

  /**
   * Reference to the actual resender that does the work of resending the messages
   */
  private Resender            resender;

  /**
   * Boolean that indicates whether we have started monitoring yet
   */
  private boolean             startedMonitoring = false;

  /**
   * Creates a new BackupMonitor with the specified CacheDir and URL for sending logs to
   * 
   * @param cacheDirPath
   *          String containing path of cache dir
   * @param url
   *          String containing server URL
   */
  public BackupMonitor(String cacheDirPath, String url) {
    logger.info("Creating new backup monitor at {}, with url: {}", cacheDirPath, url);
    this.cacheDir = new File(cacheDirPath);
    if (!this.cacheDir.exists()) {
      if (!this.cacheDir.mkdirs()) {
        logger.warn("Creation of cache directory failed");
      }

    }
    this.resendRecords = new File(this.cacheDir, RESEND_RECORDS);
    if (!this.resendRecords.exists()) {
      if (!this.resendRecords.mkdirs()) {
        logger.warn("Creation of resend records directory failed");
      }
    }
    this.resender = new Resender(url);
    this.reloadDataFromDisk();
  }

  /**
   * Creates a new BackupMonitor with the specified CacheDir and URL for sending logs to. Also includes the CloseableHttpClient to
   * use for resending, which will be passed to the Resender
   * 
   * @param cacheDirPath
   *          String containing path of cache dir
   * @param url
   *          String containing server URL
   * @param client
   *          CloseableHttpClient to use for sending the requests
   */
  public BackupMonitor(String cacheDirPath, String url, CloseableHttpClient client) {
    logger.info("Creating new backup monitor, with specified HTTPClient, at {}, with url: {}", cacheDirPath, url);
    this.cacheDir = new File(cacheDirPath);
    if (!this.cacheDir.exists()) {
      if (!this.cacheDir.mkdirs()) {
        logger.warn("Creation of cache directory failed");
      }
    }
    this.resendRecords = new File(this.cacheDir, RESEND_RECORDS);
    if (!this.resendRecords.exists()) {
      if (!this.resendRecords.mkdirs()) {
        logger.warn("Creation of resend records directory failed");
      }
    }
    this.resender = new Resender(client, url);
    this.reloadDataFromDisk();
  }

  /**
   * Initialises a new backup file. This performed synchronously since there can only be one backup file in use at any one time
   * 
   * @throws IOException
   * @throws ResendFileException
   */
  private synchronized void initBackupFile() throws IOException, ResendFileException {
    this.backupFile = new BackupFile(this.cacheDir, this.resendRecords);
  }

  /**
   * Logs a messages to the underlying backup file, creating and initialising a new one if not backup file is currently in use.
   * 
   * @param line
   *          String containing the message to log
   * @throws ResendFileException
   */
  public synchronized void logMessage(String line) throws ResendFileException {
    try {
      if (this.backupFile == null) {
        this.initBackupFile();
      }
      this.backupFile.write(line);
    }
    catch (IOException e) {
      LogValidity.getInstance().taintLog();
      logger.warn("Exception writing logging message to file", e);
      throw new ResendFileException("Exception whilst writing to backup file", e);

    }
  }

  /**
   * Checks whether the resendRecords directory contains any files, if it does, it indicates there are logs to resend
   * 
   * @return boolean true if there are logs present on the disk, false if there are not
   */
  public boolean logsOnDisk() {
    return (this.resendRecords.list().length > 0);
  }

  /**
   * Reloads any existing data from disk in preparation for resending. This process creates a new ResendRecord and passes it to the
   * Resender for immediate resending as soon as successful send has taken place
   */
  private synchronized void reloadDataFromDisk() {
    logger.info("Checking for past logs to reload from disk");
    File[] diskRecords = this.resendRecords.listFiles();
    for (File diskRecord : diskRecords) {
      if (diskRecord.exists() && diskRecord.isFile() && diskRecord.getName().endsWith(ResendRecord.RESEND_SUFFIX)
          && diskRecord.getName().startsWith(ResendRecord.RESEND_PREFIX)) {
        try {
          logger.info("Reloading ResendRecord:" + diskRecord.getAbsolutePath());
          ResendRecord reloadRecord = new ResendRecord(diskRecord.getAbsolutePath());
          this.resender.addResendRecord(reloadRecord);
        }
        catch (ResendFileException e) {
          LogValidity.getInstance().taintLog();
          logger.error("Exception reloading disk record, will try to continue", e);
        }
      }
    }
  }

  /**
   * Starts the monitoring by creating a resender executors and starting it
   */
  public synchronized void startMonitoring() {
    if (!this.startedMonitoring) {
      if (this.resenderExe.isShutdown()) {
        this.resenderExe = Executors.newSingleThreadExecutor();
      }
      this.resenderExe.execute(this.resender);
      this.startedMonitoring = true;
    }
  }

  /**
   * Stops monitoring by immediatelt shutting down the resender executor service
   */
  public synchronized void stopMonitoring() {
    this.resenderExe.shutdownNow();
    this.startedMonitoring = false;
  }

  /**
   * SuccessfulSend is called when a log message has been successfully sent to the server, somewhere else in the system. This
   * triggers resending to start/restart if there are any messages queued for sending. It also closes any existing backup file,
   * since we are now back to successful sends, we should not need the backup file.
   * 
   * @throws IOException
   * @throws ResendFileException
   */
  public synchronized void successfulSend() throws IOException, ResendFileException {
    if (this.backupFile != null) {
      logger.info("Successful sent received, closing backup file");
      this.backupFile.close();
      String resendRecordPath = this.backupFile.getResendRecordPath();
      this.backupFile = null;
      ResendRecord resendRecord = new ResendRecord(resendRecordPath);
      this.resender.addResendRecord(resendRecord);
    }
    this.resender.sendSuccessful();
  }
}
