/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders.filebackedqueue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import custom.appenders.exceptions.ResendFileException;

/**
 * Keeps a reference to a backup file and is used to write log message to it. The underlying file is created using createTempFile to
 * ensure that it is unique. Additionally, the backup file creates, and keeps a reference to, the ResendRecord. This is a separate
 * file that contains the path of the underlying backup file and how many lines of it have already been resent.
 * 
 * @author Chris Culnane
 * 
 */
public class BackupFile {

  /**
   * BufferedWriter for writing the logging events to the underlyng file
   */
  private BufferedWriter bw;

  /**
   * String path to the ResendRecord
   */
  private String         resendRecordPath;

  /**
   * Constructs a new BackupFile and ResendRecord. A BufferedWriter is created and opened. This should be treated as a File stream
   * that needs closing once it is finished with
   * 
   * @param directory
   *          File directory to store the backup file
   * @param resendRecordDir
   *          File resend directory where ResendRecords are stored
   * @throws IOException
   * @throws ResendFileException
   */
  public BackupFile(File directory, File resendRecordDir) throws IOException, ResendFileException {
    File backupFile = File.createTempFile("httpAppend", ".logbackup", directory);
    this.bw = new BufferedWriter(new FileWriter(backupFile));
    this.resendRecordPath = ResendRecord.createNewRecord(backupFile, resendRecordDir);
  }

  /**
   * Closes the underlying stream
   * 
   * @throws IOException
   */
  public void close() throws IOException {
    this.bw.close();
  }

  /**
   * Gets the path to the ResendRecord
   * 
   * @return
   */
  public String getResendRecordPath() {
    return this.resendRecordPath;
  }

  /**
   * Writes a line to the file based log file and flushes the contents
   * 
   * @param line
   *          String line to write
   * @throws IOException
   */
  public void write(String line) throws IOException {
    this.bw.write(line);
    this.bw.write("\n");
    this.bw.flush();
  }
}
