/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders.filebackedqueue;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import custom.appenders.LogValidity;
import custom.appenders.exceptions.ResendFileException;

/**
 * Resender performs the actual work of resending log messages to the server. It always uses a Synchronous HTTP Client and attempts
 * to send one log message at a time, only moving onto the next one once the previous has been successfully sent.
 * 
 * @author Chris Culnane
 * 
 */
public class Resender implements Runnable {

  /**
   * Static logger
   */
  private static final Logger               logger         = LoggerFactory.getLogger(Resender.class);
  /**
   * Queue of ResendRecords that need sending. These are relatively small objects, so shouldn't grow by too much
   */
  private LinkedBlockingQueue<ResendRecord> resendQueue    = new LinkedBlockingQueue<ResendRecord>();

  /**
   * HttpClient to use for sending the post requests. If no HttpClient is specified a new one is created
   */
  private CloseableHttpClient               httpClient;

  /**
   * String containing the server URL
   */
  private String                            url;

  /**
   * Boolean that indicates if a successful send has taken place
   */
  private volatile boolean                  successfulSend = false;

  /**
   * Constructs a new Resender with a specified HTTPClient and url
   * 
   * @param httpClient
   *          CloseableHttpClient to use for sending requests
   * @param url
   *          String url to send requests to
   */
  public Resender(CloseableHttpClient httpClient, String url) {
    logger.info("New Resender created with url: {}, httpClient specified, will use that one", url);
    this.httpClient = httpClient;
    this.url = url;
  }

  /**
   * Constructs a new resender with the specified url, a new HttpClient will be created for it to use
   * 
   * @param url
   *          String url to send requests to
   */
  public Resender(String url) {
    logger.info("New Resender created with url: {} no HttpClient specified, will create a new one", url);
    this.httpClient = HttpClients.createDefault();
    this.url = url;
  }

  /**
   * Adds a ResendRecord to the queue of backup files that need resending
   * 
   * @param record
   *          ResendRecord to add to the queue
   */
  public void addResendRecord(ResendRecord record) {
    this.resendQueue.add(record);
    logger.info("New resend record added to queue");
  }

  /**
   * Synchronized method that causes execution to stop until a successful send has taken place and this thread has been notified.
   * The reason it is in a loop is due to false wake ups that can occur in the JVM
   * 
   * @throws InterruptedException
   */
  private synchronized void checkSendStatus() throws InterruptedException {
    while (!this.successfulSend) {
      this.wait();
    }
  }

  /**
   * Loops forever trying to send any queued ResendRecords. It initially checks that the last send was successful, if not, it waits
   * until a successful send has taken place. It then waits for a ResendRecord to be present in the queue. Once a record exists it
   * removes it from the queue and calls readAndSend until either a FAIL or COMPLETE response is received.
   * 
   * If a FAIL response is received we treat it like a wider send fail and stop until a successful send in the wider logger
   * framework. If COMPLETE we clean-up the record and loop back to check the queue again.
   * 
   * The actual sending is performed by the ResendRecord itself. It handles trying to resend the same line if failures take place.
   * 
   */
  @Override
  public void run() {
    try {
      try {
        while (true) {
          this.checkSendStatus();
          ResendRecord currentRecord = this.resendQueue.take();
          boolean keepSending = true;
          while (keepSending) {
            ReadAndSendResult result = currentRecord.readAndSend(this.httpClient, this.url);
            if (result == ReadAndSendResult.FAIL) {
              // We failed to send so stop sends until success
              this.sendFailure();
              // Wait for success
              this.checkSendStatus();
            }
            else if (result == ReadAndSendResult.COMPLETE) {
              currentRecord.cleanup();
              // clean up files
              keepSending = false;
            }
          }
        }
      }
      catch (ResendFileException e) {
        logger.error("Exception whilst trying to resend record", e);
        LogValidity.getInstance().taintLog();
      }
    }
    catch (InterruptedException e) {
      logger.warn("Resender interrupted, this indicates we are probably shutting down");
    }

  }

  /**
   * Indicates a send has failed and will cause future sends of the resender to be paused until a successful send is received
   */
  public synchronized void sendFailure() {
    this.successfulSend = false;
  }

  /**
   * Indicates a successful send has taken place, updates the boolean value and then notifies the thread so that it can start
   * resending again, if there is anything to resend
   */
  public synchronized void sendSuccessful() {
    this.successfulSend = true;
    this.notify();
  }
}
