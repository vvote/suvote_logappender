/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders;

import org.json.JSONObject;

import ch.qos.logback.core.LayoutBase;

/**
 * JsonTeleLayoutBase is the base layout for encoding a logging event as VVA compatible JSOn
 * 
 * @author Chris Culnane
 * 
 * @param <E>
 */
public abstract class JsonTeleLayoutBase<E> extends LayoutBase<E> {

  /**
   * String containing the eventID - set in logback configuration
   */
  protected String           eventID;

  /**
   * String containing the name the message is from - set in the logback configuration
   */
  protected String           serverFrom;

  /**
   * JSON Constant for EventId field
   */
  public static final String EVENT_ID     = "EventId";

  /**
   * JSON Constant for Level field
   */
  public static final String LEVEL        = "Level";

  /**
   * JSON Constant for From field
   */
  public static final String FROM         = "From";

  /**
   * JSON Constant for MessageType field
   */
  public static final String MESSAGE_TYPE = "MessageType";

  /**
   * JSON Constant for Message field
   */
  public static final String MESSAGE      = "Message";

  /**
   * Calls the abstract toJsonObject method and checks the response is valid, returning a string representation if it is, null if
   * not
   */
  @Override
  public String doLayout(E event) {
    JSONObject resp = this.toJsonObject(event);
    if (resp == null || resp.length() == 0) {
      LogValidity.getInstance().taintLog();
      return null;
    }
    else {
      return resp.toString();
    }
  }

  /**
   * Gets the EventId
   * 
   * @return String of the EventId
   */
  public String getEventID() {
    return this.eventID;
  }

  /**
   * Gets the server that is sending the log (serverFrom)
   * 
   * @return String containing serverFrom value
   */
  public String getServerFrom() {
    return this.serverFrom;
  }

  /**
   * Sets the EventID string
   * 
   * @param eventID
   *          String containing the new EventId
   */
  public void setEventID(String eventID) {
    this.eventID = eventID;
  }

  /**
   * Sets the serverFrom field
   * 
   * @param serverFrom
   *          String containing the serverFrom valueF
   */
  public void setServerFrom(String serverFrom) {
    this.serverFrom = serverFrom;
  }

  /**
   * Abstract method to take a logging event and convert it onto a JSON Object
   * 
   * @param e
   *          LoggingEvent
   * @return JSONObject containing the converted event
   */
  protected abstract JSONObject toJsonObject(E e);

}
