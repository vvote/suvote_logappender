/*******************************************************************************
 * 
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Jayadev Devaraj - initial implementation of ServiceLogAppender 
 *     Chris Culnane - Added file backup and JSON Layouts
 ******************************************************************************/
package custom.appenders;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import custom.appenders.exceptions.HttpAppenderLogException;
import custom.appenders.exceptions.ResendFileException;
import custom.appenders.filebackedqueue.BackupMonitor;

/**
 * Service Log Appender provides an HTTP logging appender for sending Telemetry logging to VVA. It has a file backed queue, such
 * that if VVA is down the logging will queue the messages to file before resending at a later date.
 * 
 * @author Jayadev Devaraj
 * @author Chris Culnane
 * 
 */
public class ServiceLogAppender extends AppenderBase<ILoggingEvent> {

  /**
   * Inner class for handling Async heartbeat responses. Heartbeat messages are not queue if they fail - the fact they are missing
   * is an indication of a loss of connection. However, a successful heartbeat message can trigger queued messages to start sending.
   * 
   * @author Chris Culnane
   * 
   */
  class AsyncHeartbeatResponseCallback implements FutureCallback<HttpResponse> {

    /**
     * Default empty constructor
     */
    public AsyncHeartbeatResponseCallback() {
    }

    /**
     * If the request is cancelled do nothing
     */
    @Override
    public void cancelled() {
      ServiceLogAppender.this.addWarn("Heartbeat sending cancelled");
    }

    /**
     * If the request completes with a 200 response we can indicate a successful send and trigger resending of queued messages
     */
    @Override
    public void completed(HttpResponse response) {
      try {
        // Gets the HTTPEntity, consumes the content
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          EntityUtils.consumeQuietly(entity);
        }

        // Check the response code
        if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 202) {
          ServiceLogAppender.this.getBackupMonitor().successfulSend();
        }
        else {
          ServiceLogAppender.this.addWarn("Heartbeat sending failed:" + response.getStatusLine().getStatusCode());
        }
      }
      catch (IOException e) {
        ServiceLogAppender.this.addWarn("Heartbeat sending failed:" + e.getMessage());
      }
      catch (ResendFileException e) {
        ServiceLogAppender.this.addError("Heartbeat successful, restart of sending failed", e);
      }

    }

    /**
     * If the request fails we will do nothing
     */
    @Override
    public void failed(Exception exception) {
      ServiceLogAppender.this.addError("Exception sending heartbeat:" + exception.getMessage());
    }

  }

  /**
   * Async Response callback for logging messages that have been sent with the Asyc Http Client. If this fails it will switch the
   * logging over to file based and will wait for a successful message before resending.
   * 
   * @author Chris Culnane
   * 
   */
  class AsyncResponseCallback implements FutureCallback<HttpResponse> {

    /**
     * String containing the underlying log message we are trying to send
     */
    private String logMessage;

    /**
     * Construct a new AsyncResponseCallback with the string containing the messages being logged
     * 
     * @param logMessage
     *          String containing log message
     */
    public AsyncResponseCallback(String logMessage) {
      this.logMessage = logMessage;
    }

    /**
     * If the request is cancelled this constitutes an error. We log the message to the file. If an exception happens whilst trying
     * to fall back to file based logging we consider the message to have been lost and taint the log to indicate we can't be sure
     * it is complete
     */
    @Override
    public void cancelled() {
      try {
        // Log message to backup monitor
        ServiceLogAppender.this.getBackupMonitor().logMessage(this.logMessage);
      }
      catch (ResendFileException e) {
        // This means an exception occurred when backing up the log, we taint the log to indicate it may not be complete
        ServiceLogAppender.this.addError("Exception whilst trying to log message to backup", e);
        LogValidity.getInstance().taintLog();
      }

    }

    /**
     * This means we have successfully communicated with the server, but we still need to check that the appropriate response code
     * was received.
     */
    @Override
    public void completed(HttpResponse response) {
      try {
        // Consume the entity
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          EntityUtils.consumeQuietly(entity);
        }

        // Check the status returned is 200
        if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 202) {
          // Indicate a successful send to the backup monitor
          ServiceLogAppender.this.getBackupMonitor().successfulSend();
        }
        else {
          // Log the message to the backup monitor
          ServiceLogAppender.this.getBackupMonitor().logMessage(this.logMessage);
        }
      }
      catch (IOException e) {
        // This indicates an exception has taken place, possibly in the response, try to back up the message
        try {
          ServiceLogAppender.this.getBackupMonitor().logMessage(this.logMessage);
        }
        catch (ResendFileException e1) {
          // If an exception has occurred whilst backing up we may have lost the message, taint the log
          ServiceLogAppender.this.addError("Exception whilst trying to log message to backup", e);
          LogValidity.getInstance().taintLog();
        }
      }
      catch (ResendFileException e) {
        // If an exception has occurred whilst backing up we may have lost the message, taint the log
        ServiceLogAppender.this.addError("Exception whilst trying to log message to backup", e);
        LogValidity.getInstance().taintLog();
      }

    }

    /**
     * The send failed, try to backup the log message
     */
    @Override
    public void failed(Exception exception) {
      try {
        ServiceLogAppender.this.getBackupMonitor().logMessage(this.logMessage);
      }
      catch (ResendFileException e) {
        // If an exception has occurred whilst backing up we may have lost the message, taint the log
        ServiceLogAppender.this.addError("Exception whilst trying to log message to backup", e);
        LogValidity.getInstance().taintLog();
      }
    }

  }

  /**
   * Simple runnable class that creates and send the heartbeat message at periodical intervals
   * 
   * @author Chris Culnane
   * 
   */
  class LogHeartbeat implements Runnable {

    /**
     * Default empty constructor
     */
    public LogHeartbeat() {
    }

    /**
     * Run method where the heartbeat message is constructed and sent. This attempts to construct a well formed heartbeat message,
     * but if something is wrong it will construct a hard-coded string heartbeat with as much information as is likely to be
     * available
     */
    @Override
    public void run() {
      try {
        // Get a complete heartbeat message
        ServiceLogAppender.this.sendHeartbeat(((JsonTeleLayout) ServiceLogAppender.this.encoder.getLayout())
            .getHeartbeatObject(ServiceLogAppender.this.getBackupMonitor().logsOnDisk()));
      }
      catch (Exception e) {
        try {
          // If an exception has occurred something from and we should consider the log tainted
          ServiceLogAppender.this.sendHeartbeat("{\"EventId\":\"Unknown\",\"From\":\"Unknown\",\"Level\":\"" + "WARN"
              + "\",\"MessageType\":\"" + JsonTeleLayout.HEARTBEAT_MSGTYPE + "\",\"Message\":\"" + "Telemetry Tainted#"
              + System.currentTimeMillis() + "\"}");
        }
        catch (HttpAppenderLogException e1) {
          // If this exception occurs it means we couldn't even construct a valid post request, we therefore taint the log because
          // this will impact on everything
          ServiceLogAppender.this.addError("Exception creating heartbeat", e1);
          LogValidity.getInstance().taintLog();
        }
      }
    }

  }

  /**
   * User-agent string
   */
  final String                                   USER_AGENT         = "Mozilla/5.0";

  /**
   * Encoder that wraps a JsonTeleLayout for producing the JSON from the log message
   */
  protected LayoutWrappingEncoder<ILoggingEvent> encoder;

  /**
   * ByteArrayOutputStream that is used to write encoded data to an array for sending via HTTPClient
   */
  private ByteArrayOutputStream                  baos               = new ByteArrayOutputStream();

  /**
   * Executor service for the heartbeat messages
   */
  private ScheduledExecutorService               heartbeatExe       = Executors.newSingleThreadScheduledExecutor();

  /**
   * Boolean value, set in logback.xml, that determines whether the log messages should be sent with an async http client (true)
   * should be quicker
   */
  private boolean                                asyncClient        = false;

  /**
   * boolean that indicates if we should use SSL for connecting to the server
   */
  private boolean                                useSSL             = false;

  /**
   * Default delay between heartbeat messages - configurable in logback.xml
   */
  private int                                    heartbeatDelay     = 60000;

  /**
   * String path to the trust stroe
   */
  protected String                               trustStorePath;

  /**
   * String password to the truststore
   */
  protected String                               trustStorePassword = "";

  /**
   * String serverURL set in the logback.xml
   */
  protected String                               serverURL;

  /**
   * String path to the cache directory to use for queuing messages
   */
  protected String                               cacheDir;

  /**
   * BackupMonitor handles saving logs to file and resending them when a connection is available
   */
  private volatile BackupMonitor                 backupMonitor;

  /**
   * Synchronous HTTP Client to use when sending messages - this is reused throughout the logging. This will always be used by the
   * BackupMonitor, which only sends synchronous requests even if the logger is asynchronous
   */
  private volatile CloseableHttpClient           httpClient         = null;

  /**
   * Asynchronouse HTTP Client for the serviceLogAppender to use if async is set to true
   */
  private volatile CloseableHttpAsyncClient      httpAsyncClient    = null;

  /**
   * Appends the event by first encoding it into a JSON String. It then is sent, via a post request, to the server
   */
  @Override
  public void append(ILoggingEvent event) {
    try {
      this.encoder.doEncode(event);
      String sendString = this.baos.toString();
      this.baos.reset();
      this.sendPost(sendString);
    }
    catch (IOException e) {
      this.addError("IOException whilst trying to encode log message", e);
      LogValidity.getInstance().taintLog();

    }
    catch (HttpAppenderLogException e) {
      this.addError("Exception whilst sending HTTP post of log message", e);
      LogValidity.getInstance().taintLog();
    }

  }

  /**
   * Creates the HTTPPost object we are going to send. This creates the inner string entity that contains the actual message being
   * sent and set the URL on the HttpPost
   * 
   * @param postContent
   *          String of the content to be sent (JSON)
   * @return HttpPost object that can be executed by the HttpClient
   * @throws HttpAppenderLogException
   */
  private HttpPost createPost(String postContent) throws HttpAppenderLogException {
    HttpPost postRequest = new HttpPost(this.serverURL);
    try {
      StringEntity input = new StringEntity(postContent);
      input.setContentType("application/json");

      postRequest.setEntity(input);
      return postRequest;
    }
    catch (UnsupportedEncodingException e) {
      throw new HttpAppenderLogException("Exception whilst encoding log message", e);
    }
  }

  /**
   * Gets an Asynchronous HTTP Client, calling initHttpAsyncClient if necessary to create a new one
   * 
   * This method is not synchronized, but the init method is. As such, the getting of the instance is thread-safe, with a
   * synchronized creation step on if it is necessary. The alternative would be to synchronize this method, which would unduly
   * impact on performance when getting the reference to the already created client
   * 
   * This pattern is reused for the synchronous HTTP Client and the BackupMonitor
   * 
   * @return CloseableHttpAsyncClient async client to use to send HTTP Posts
   */
  private CloseableHttpAsyncClient getAsyncHttpClient() {
    if (this.httpAsyncClient == null) {
      this.initHttpAsyncClient();
    }
    return this.httpAsyncClient;
  }

  /**
   * Gets the instance of the BackupMonitor, calling initBackupMonitor if no BackupMonitor exists yet
   * 
   * @return instance of the BackupMonitor
   */
  private BackupMonitor getBackupMonitor() {
    if (this.backupMonitor == null) {
      this.initBackupMonitor();
    }
    return this.backupMonitor;
  }

  /**
   * Gets the user set cache dir string - this where the file caches the log queue
   * 
   * @return String containing the set cache path
   */
  public String getCacheDir() {
    return this.cacheDir;
  }

  /**
   * Gets the encoder to use for this appender, again set in the logback configuration
   * 
   * @return LayoutWrappingEncoder that wraps the JsonTeleLayout and is used to encode the messages
   */
  public LayoutWrappingEncoder<ILoggingEvent> getEncoder() {
    return this.encoder;
  }

  /**
   * Gets the int heartbeat delay that is set in the logback configuration. This is the time between heartbeat messages being sent
   * 
   * @return int of the heartbeat delay in seconds
   */
  public int getHeartbeatDelay() {
    return this.heartbeatDelay;
  }

  /**
   * Gets the instance of the HttpClient to use for sending post requests to the server. The same instance is used throughout,
   * including by the BackupMonitor for resending queued logging messages
   * 
   * @return CloseableHttpClient to use for sending Http Posts
   */
  private CloseableHttpClient getHttpClient() {
    if (this.httpClient == null) {
      this.initHttpClient();
    }
    return this.httpClient;
  }

  /**
   * Gets the ServerURL for sending the posts to - configured in logback.xml
   * 
   * @return String containing the server URL
   */
  public String getServerURL() {
    return this.serverURL;
  }

  /**
   * Gets the trust store password to use
   * 
   * @return String containing the truststore password
   */
  public String getTrustStorePassword() {
    return this.trustStorePassword;
  }

  /**
   * Gets the path to the truststore
   * 
   * @return String path to truststore
   */
  public String getTrustStorePath() {
    return this.trustStorePath;
  }

  /**
   * Should we use SSL to connect with
   * 
   * @return the useSSL
   */
  public boolean getUseSSL() {
    return this.useSSL;
  }

  /**
   * Synchronized method to initialise the BackupMonitor.
   */
  private synchronized void initBackupMonitor() {
    if (this.backupMonitor == null) {
      this.backupMonitor = new BackupMonitor(this.getCacheDir(), this.getServerURL(), this.getHttpClient());
      this.backupMonitor.startMonitoring();
    }

  }

  /**
   * Creates a new httpAsyncClient if it doesn't already exist. We re-check the null value since this method is called from an
   * unsynchronized method and there could be multiple concurrent calls to the method. By re-checking the null guard we can
   * guarantee only a single instance will be created.
   */
  private synchronized void initHttpAsyncClient() {
    if (this.httpAsyncClient == null) {
      if (this.getUseSSL()) {
        this.httpAsyncClient = HttpAsyncClients.custom().setSSLContext(this.loadSSLContext())
            .setHostnameVerifier(SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();
      }
      else {
        this.httpAsyncClient = HttpAsyncClients.createDefault();
      }
      this.httpAsyncClient.start();
    }
  }

  /**
   * Synchronized method for initialising the HTTP Client in a thread safe manner
   */
  private synchronized void initHttpClient() {
    if (this.httpClient == null) {
      if (this.getUseSSL()) {
        this.httpClient = HttpClients.custom().setSSLSocketFactory(this.loadSSLSocketFactory()).build();
      }
      else {
        this.httpClient = HttpClients.createDefault();
      }
    }

  }

  /**
   * Loads the SSLContext using the keystore and password set in the logback config. We separate out the creation of the SSLContext
   * from the SSLSocketFactory to allow resue of the code by both the Async and synchronous HttpClients, which have slightly
   * different initialisations for SSL.
   * 
   * The specified KeyStore should contain both the client key and the server ca certifcate, since it will act as both a KeyStore
   * and a TrustStore
   * 
   * @return SSLContext using the specified keystore
   */
  private SSLContext loadSSLContext() {
    FileInputStream instream = null;
    try {
      // Load keystores
      KeyStore mykeys = KeyStore.getInstance(KeyStore.getDefaultType());
      instream = new FileInputStream(new File(this.getTrustStorePath()));
      mykeys.load(instream, this.getTrustStorePassword().toCharArray());

      // Create SSLContext
      SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(mykeys, new TrustSelfSignedStrategy())
          .loadKeyMaterial(mykeys, this.getTrustStorePassword().toCharArray()).build();
      return sslcontext;
    }
    catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException | KeyManagementException
        | UnrecoverableKeyException e) {
      e.printStackTrace();
      this.addError("FATAL Error cannot open Keystores:" + e.getMessage());
    }
    finally {
      if (instream != null) {
        try {
          instream.close();
        }
        catch (IOException e) {
          addError("Error closing keystore stream:" + e.getMessage());
        }
      }
    }
    return null;

  }

  /**
   * Loads the SSLSocketFactory with a newly created SSLContext
   * 
   * @return SSLConnectionSocketFactory for use by the HTTPClient
   */
  private SSLConnectionSocketFactory loadSSLSocketFactory() {
    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(this.loadSSLContext(),
        SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    return sslsf;
  }

  /**
   * Checks if we should send the request asynchronously or not
   * 
   * @return boolean, true if the asynchronous, false if synchronous
   */
  public boolean isAsyncClient() {
    return this.asyncClient;
  }

  /**
   * Sends the heartbeat message using either tha Async HttpClient of the synchronous one, depending on the user configuration
   * 
   * @param formattedLog
   *          String containing the heartbeat message to send
   * @throws HttpAppenderLogException
   */
  private void sendHeartbeat(String formattedLog) throws HttpAppenderLogException {
    if (this.asyncClient) {
      // We are sending asynchronously - execute with new callback
      this.getAsyncHttpClient().execute(this.createPost(formattedLog), new AsyncHeartbeatResponseCallback());
    }
    else {
      // We are sending synchronously - execute and wait for response
      try {
        CloseableHttpResponse response = this.getHttpClient().execute(this.createPost(formattedLog));
        // Consume the entity
        try {
          HttpEntity entity = response.getEntity();
          if (entity != null) {
            EntityUtils.consumeQuietly(entity);
          }
        }
        finally {
          // Close the response
          response.close();
        }
        // Check response code - and mark it either successful or log the error
        if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 202) {
          this.getBackupMonitor().successfulSend();
        }
        else {
          this.addWarn("Heartbeat sending failed");
        }
      }
      catch (IOException e) {
        this.addWarn("Heartbeat sending failed");
      }
      catch (ResendFileException e) {
        this.addError("Heartbeat successful, restart of sending failed", e);
      }
    }
  }

  /**
   * Attempts to send the formatted log message (JSON String) to the server via an appropriate HTTP Client. If it fails the message
   * will be passed onto the BackupMonitor for saving to file for later resending
   * 
   * @param formattedLog
   *          String of the JSON message to send
   * @throws HttpAppenderLogException
   */
  private void sendPost(String formattedLog) throws HttpAppenderLogException {
    if (this.asyncClient) {
      // Send via the async client, using the specified response callback
      this.getAsyncHttpClient().execute(this.createPost(formattedLog), new AsyncResponseCallback(formattedLog));
    }
    else {
      // Send via the synchronous client
      try {
        CloseableHttpResponse response = this.getHttpClient().execute(this.createPost(formattedLog));

        // Consume the entity
        try {
          HttpEntity entity = response.getEntity();
          if (entity != null) {
            EntityUtils.consumeQuietly(entity);
          }
        }
        finally {
          // Close the response
          response.close();
        }

        // Check the response code
        if (response.getStatusLine().getStatusCode() >= 200 && response.getStatusLine().getStatusCode() <= 202) {
          // Successfully sent, notify backup monitor so it can send any queued messages
          this.getBackupMonitor().successfulSend();
        }
        else {
          // Send failed, log the message to file
          this.getBackupMonitor().logMessage(formattedLog);
        }
      }
      catch (IOException e) {
        try {
          // An IOexception occurred - server could be offline, log message to file
          this.getBackupMonitor().logMessage(formattedLog);
        }
        catch (ResendFileException e1) {
          throw new HttpAppenderLogException("Exception whilst trying to prepare backup", e1);
        }
      }
      catch (ResendFileException e) {
        throw new HttpAppenderLogException("Exception whilst trying to prepare BackupMonitor", e);
      }
    }
  }

  /**
   * Sets whether we should use the async client or not
   * 
   * @param asyncClient
   *          true if we should use an async client, false if not
   */
  public void setAsyncClient(boolean asyncClient) {
    this.asyncClient = asyncClient;
  }

  /**
   * Sets the cache directory to use
   * 
   * @param cacheDir
   *          String path to the cacheDir
   */
  public void setCacheDir(String cacheDir) {
    this.cacheDir = cacheDir;
  }

  /**
   * Sets the encoder to use for encoding messages. We expect it to be a LayoutWrappingEncoder that wraps a JsonTeleLayout
   * 
   * @param encoder
   *          LayoutWrappingEncoder that performs the encoding
   */
  public void setEncoder(LayoutWrappingEncoder<ILoggingEvent> encoder) {
    this.encoder = encoder;
  }

  /**
   * Sets the delay, in seconds, between heartbeat messages
   * 
   * @param heartbeatDelay
   *          int delay, in seconds, between heartbeat messages
   */
  public void setHeartbeatDelay(int heartbeatDelay) {
    this.heartbeatDelay = heartbeatDelay;
  }

  /**
   * Sets the server url to use when sending the logs
   * 
   * @param serverURL
   *          String containing the server url
   */
  public void setServerURL(String serverURL) {
    this.serverURL = serverURL;
  }

  /**
   * Sets the truststore password
   * 
   * @param trustStorePassword
   *          String containing the truststore password
   */
  public void setTrustStorePassword(String trustStorePassword) {
    this.trustStorePassword = trustStorePassword;
  }

  /**
   * Sets the path to the truststore
   * 
   * @param trustStorePath
   *          String path to the truststore
   */
  public void setTrustStorePath(String trustStorePath) {
    this.trustStorePath = trustStorePath;
  }

  /**
   * Sets whether we should use SSL for connecting to the host
   * 
   * @param useSSL
   *          the useSSL to set
   */
  public void setUseSSL(boolean useSSL) {
    this.useSSL = useSSL;
  }

  /**
   * Previous SSL Code - left in for reference
   */
  /*
   * { KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType()); InputStream trustStore = new
   * FileInputStream(trustStorePath); keyStore.load(trustStore, trustStorePassword.toCharArray()); TrustManagerFactory tmf =
   * TrustManagerFactory .getInstance(TrustManagerFactory.getDefaultAlgorithm()); tmf.init(keyStore); SSLContext ctx =
   * SSLContext.getInstance("TLS"); ctx.init(null, tmf.getTrustManagers(), null); SSLSocketFactory sslFactory =
   * ctx.getSocketFactory();
   * 
   * String url = serverURL; URL obj = new URL(url); HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
   * 
   * // add request header con.setSSLSocketFactory(sslFactory); con.setRequestMethod("POST"); con.setRequestProperty("User-Agent",
   * USER_AGENT); con.setRequestProperty("Accept", "application/json"); con.setRequestProperty("Content-Type", "application/json");
   * 
   * 
   * JSONObject jsonObj = new JSONObject();
   * 
   * jsonObj.put("EventId", encoder.getPattern().split(",")[0]); jsonObj.put("Level", event.getLevel()); jsonObj.put("From",
   * encoder.getPattern().split(",")[1]); jsonObj.put("MessageType", event.getFormattedMessage().split("#")[0]);
   * jsonObj.put("Message", event.getFormattedMessage().split("#")[1]);
   * 
   * String urlParameters = jsonObj.toString();
   * 
   * // Send post request con.setDoOutput(true); DataOutputStream wr = new DataOutputStream(con.getOutputStream());
   * //wr.writeBytes(urlParameters); wr.writeBytes(urlParameters); wr.flush(); wr.close();
   * 
   * int responseCode = con.getResponseCode(); System.out.println(String.format(
   * "Response Code: %d 'POST' request to URL : %s, Message: %s", responseCode, url, urlParameters));
   * 
   * BufferedReader in = new BufferedReader(new InputStreamReader( con.getInputStream())); String inputLine; StringBuffer response =
   * new StringBuffer();
   * 
   * while ((inputLine = in.readLine()) != null) { response.append(inputLine); } in.close(); }
   */

  /**
   * Starts the log appender and initialises an executor to send heartbeat messages. Initialises the encoder with a
   * ByteArrayOutputStream to get the content from
   */
  @Override
  public void start() {

    if (this.encoder == null) {
      this.addError("No encoder set for the appender named [" + this.name + "].");
      return;
    }
    try {
      this.encoder.init(this.baos);
    }
    catch (IOException e) {
      this.addError("Exception whilst initialising encoder", e);
      LogValidity.getInstance().taintLog();
    }
    super.start();
    this.heartbeatExe.scheduleWithFixedDelay(new LogHeartbeat(), this.getHeartbeatDelay(), this.getHeartbeatDelay(),
        TimeUnit.SECONDS);
  }

  /**
   * Called when shutting down the appender. We stop the BackupMonitor and close and stop any HTTP clients. We also force stop the
   * heartbeat executor service
   */
  @Override
  public void stop() {
    super.stop();
    this.getBackupMonitor().stopMonitoring();
    if (this.httpClient != null) {
      try {
        this.httpClient.close();
      }
      catch (IOException e) {
        this.addError("Exception whilst clsoing HTTPClient", e);
      }
    }
    if (this.httpAsyncClient != null) {
      try {
        this.httpAsyncClient.close();
      }
      catch (IOException e) {
        this.addError("Exception whilst closing HTTPAsyncClient", e);
      }
    }
    this.heartbeatExe.shutdownNow();
  }

}
