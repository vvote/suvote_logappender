/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.NOPLogger;

import ch.qos.logback.core.status.Status;
import ch.qos.logback.core.status.StatusListener;

/**
 * LoggingStatusListener listens for the internal status messages from the logback framework. It then attempts to log these to a
 * standard logger. The logger is lazily constructed to prevent errors in trying to write to a logger that hasn't yet been
 * initialised.
 * 
 * @author Chris Culnane
 * 
 */
public class LoggingStatusListener implements StatusListener {

  /**
   * Instance of logger that will be initialised as soon as possible
   */
  private Logger logger = null;

  /**
   * Default empty constructor
   */
  public LoggingStatusListener() {
  }

  /**
   * Adds a status message to the logger
   */
  @Override
  public void addStatusEvent(Status status) {
    // If the logger is null and the origin of the message is not internal to logback we try to create a new logger. We check the
    // origin because otherwise we keep trying to create log messages during construction
    
    if (logger == null && !status.getOrigin().toString().startsWith("ch.qos") && !status.getOrigin().toString().startsWith("c.q")) {
      Logger tempLogger = LoggerFactory.getLogger(LoggingStatusListener.class);
      if (!(tempLogger instanceof NOPLogger)) {
        logger = tempLogger;
      }
    }
    
    // If the logger is still null write the contents to the System.out
    if (logger == null) {
      System.out.println(status.toString());
    }
    else {
      // We have a logger, so now write to it
      switch (status.getLevel()) {
        case Status.INFO:
          logger.info("{}-{}", status.getOrigin(), status.getMessage());
          break;
        case Status.WARN:
          logger.warn("{}-{}", status.getOrigin(), status.getMessage());
          break;
        case Status.ERROR:
          logger.error("{}-{}", status.getOrigin(), status.getMessage());
          break;
      }
    }

  }

}
