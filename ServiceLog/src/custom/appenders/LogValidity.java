/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple singleton to keep track of whether the log sending could be tainted. It is considered tainted if there is a chance a
 * record has been lost. As such, if an exception occurs anywhere within the appender or backup process, that could impact on the
 * safe handling of a log message, the log will be tainted. Once the log is tainted that information is passed onto the server in
 * the heartbeat message
 * 
 * @author Chris Culnane
 * 
 */
public class LogValidity {

  /**
   * Static logger
   */
  private static final Logger logger            = LoggerFactory.getLogger(LogValidity.class);
  /**
   * Volatile boolean value that determines whether the log is tainted - if true, it indicates we may have lost some log messages
   * due to exceptions. This will not be true if the loss is due to a restart of the system
   */
  private volatile boolean         tainted   = false;
  
  /**
   * Private static instance of the singleton
   */
  private static final LogValidity _instance = new LogValidity();

  /**
   * Private constructor, the only valid way to get an instance is via getInstance()
   */
  private LogValidity() {

  }

  /**
   * Gets the static instance of LogValidity
   * @return LogValidity
   */
  public static final LogValidity getInstance() {
    return _instance;
  }

  /**
   * Checks if the log is marked as tainted - this indicates that an exception has occurred at some point that calls into questions
   * the completeness of the log. This information is passed onto the server in the heartbeat message. Note: this is obvisouly reset
   * during a restart, which indicates it only covers detectable problems in the currently running session
   * 
   * @return boolean, true if log might be incomplete, false if no known reason for incompleteness has been detected
   */
  public boolean isTainted() {
    return this.tainted;
  }

  /**
   * Marks the log as tainted
   */
  public void taintLog() {
    logger.warn("Marking log as tainted");
    this.tainted = true;
  }
}
