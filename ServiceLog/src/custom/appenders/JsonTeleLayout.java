/*******************************************************************************
 * Copyright (c) 2014 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package custom.appenders;

import org.json.JSONException;
import org.json.JSONObject;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;

/**
 * A JSON based Layout that converts the logging event into the appropriate JSON object
 * 
 * @author Chris Culnane
 * 
 */
public class JsonTeleLayout extends JsonTeleLayoutBase<ILoggingEvent> {

  /**
   * Constant for the MessageType field.
   */
  public static final String HEARTBEAT_MSGTYPE = "TELEMETRYINFO";

  /**
   * Gets a new Heartbeat object using the configured information in the layout. Also adds the additional information specified by
   * the parameters
   * 
   * @param logsOnDisk
   *          boolean, true if there are current logs waiting to be sent
   * @return String containing the JSON to send
   */
  protected String getHeartbeatObject(boolean logsOnDisk) {
    JSONObject resp = new JSONObject();
    try {
      // Attempt to construct a meaningful message
      resp.put(EVENT_ID, this.getEventID());
      resp.put(FROM, this.getServerFrom());
      resp.put(LEVEL, "DEBUG");
      resp.put(MESSAGE_TYPE, HEARTBEAT_MSGTYPE);
      if (!LogValidity.getInstance().isTainted()) {
        resp.put(MESSAGE, "Telemetry Heartbeat#" + System.currentTimeMillis() + "#" + logsOnDisk);
      }
      else {
        resp.put(MESSAGE, "Telemetry Tainted#" + System.currentTimeMillis() + "#" + logsOnDisk);
      }
      return resp.toString();
    }
    catch (JSONException e) {
      // If something has gone wrong fall back to a pure string construction
      return "{\"" + EVENT_ID + "\":\"" + this.getEventID() + "\",\"" + FROM + "\":\"" + this.getServerFrom() + "\",\"" + LEVEL
          + "\":\"" + "DEBUG" + "\",\"" + MESSAGE_TYPE + "\":\"" + HEARTBEAT_MSGTYPE + "\",\"" + MESSAGE + "\":\""
          + "Telemetry Tainted" + "\"}";

    }
  }

  /**
   * Converts a logging event into a JSONObject using the fixed formatting used by the VVA logging
   */
  @Override
  protected JSONObject toJsonObject(ILoggingEvent event) {
    JSONObject resp = new JSONObject();
    try {
      resp.put(EVENT_ID, this.getEventID());
      resp.put(FROM, this.getServerFrom());

      Level level = event.getLevel();
      if (level != null) {
        String lvlString = String.valueOf(level);
        resp.put(LEVEL, lvlString);
      }
      String[] msgSplit = event.getFormattedMessage().split("#");
      if (msgSplit.length >= 1) {
        resp.put(MESSAGE_TYPE, msgSplit[0]);
      }
      if (msgSplit.length >= 2) {
        resp.put(MESSAGE, msgSplit[1]);
      }
      return resp;
    }
    catch (JSONException e) {
      this.addError("Exception whilst converting log message to JSON", e);
      LogValidity.getInstance().taintLog();
    }
    return resp;

  }

}
